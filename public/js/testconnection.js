let connection_status = true;

$('.savebtn').attr('disabled','true');

/* Affichage du résultat à la connexion */

$('.testbtn').click(function() {

    /* Appel de la fonction de test de connection */
    $.get('/userpanel/testconnect',function(response){
        if (response) {
        $('.testbtn').html('Success <img src="img/checked.png" alt="success to connect">').addClass('res_success');
        $('.savebtn').removeAttr('disabled');

        } else {
            $('.testbtn').html('Fail <img src="img/cross.png" alt="fail to connect">').addClass('res_fail');
            $('.savebtn').attr('disabled','true');
        }
    })
});
