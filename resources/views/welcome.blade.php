<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Database Reports</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="css/style.css">

    </head>
    <body>
        <div class="flex-center position-ref full-height portal">

            <div class="content">
                <div class="title m-b-md">
                    Database Reports
                </div>

                <div class="links">
                    @if (Route::has('login'))
                        @auth
                        <a href="{{ url('/home') }}">Home</a>
                        @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                        @endauth
                    @endif
                    <a href="https://bitbucket.org/pierreher/dwreports/src" target="_blank">GitHub</a>
                </div>
            </div>
        </div>
    </body>
</html>
