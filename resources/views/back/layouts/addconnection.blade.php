<div class="row addconnection">
    <form action="#" method="post">
        <div class="form-group">
            <label for="db_host">Host :</label>
            <input type="text" class="form-control" id="db_host" name="db_host">
        </div>
        <div class="form-group">
            <label for="db_port">Port :</label>
            <input type="text" class="form-control" id="db_port" name="db_port">
        </div>
        <div class="form-group">
            <label for="db_database">Database (optionnal) :</label>
            <input type="text" class="form-control" id="db_database" name="db_database">
        </div>
        <div class="form-group">
            <label for="db_user">User :</label>
            <input type="text" class="form-control" id="db_user" name="db_">
        </div>
        <div class="form-group">
            <label for="db_password">Password :</label>
            <input type="password" class="form-control" id="db_password" name="db_password">
        </div>

        <a class="testbtn btn btn-light btn-sm">Test connection</a>

        <div class="loading-modal"></div>

        <button type="submit" class="savebtn btn btn-sm btn-dark">Save</button>

    </form>
</div>