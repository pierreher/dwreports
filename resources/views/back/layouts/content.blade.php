
<div class="row listpanel">
    <div class="col-lg-3 col-md-3 col-sm-4">
        <div class="list-group" id="list-tab" role="tablist">
            <a class="list-group-item list-group-item-action active" id="side-myconnections-list" data-toggle="list" href="#side-myconnections" role="tab" aria-controls="connections"><span class="material-icons md-24">storage</span>My Connections</a>
            <a class="list-group-item list-group-item-action" id="side-addconnection-list" data-toggle="list" href="#side-addconnection" role="tab" aria-controls="profile"><span class="material-icons md-24">add</span>Add Connection</a>
        </div>
    </div>
    <div class="col-lg-8 col-md-7 col-sm-7">
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane show active" id="side-myconnections" role="tabpanel" aria-labelledby="side-myconnections-list">

                 @include('back.layouts.myconnections')

            </div>
            <div class="tab-pane" id="side-addconnection" role="tabpanel" aria-labelledby="side-addconnection-list">

                @include('back.layouts.addconnection')

            </div>
        </div>
    </div>
</div>