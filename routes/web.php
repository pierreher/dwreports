<?php

/********************/
/*    DASHBOARD     */
/********************/

Route::get('/', function () {
    return view('welcome');
});


/********************/
/*    AUTH USER     */
/********************/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


/********************/
/*    USERPANEL     */
/********************/

Route::get('/userpanel', 'Back\PanelController@index')->name('userpanel');

/********************/
/*  ADDCONNECTION   */
/********************/

Route::get('/userpanel/testconnect','Back\ConnectionController@testConnection')->name('testconnect');